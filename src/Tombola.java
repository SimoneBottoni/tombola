import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Copyright (C) 2017-2018  Bottoni Simone - simone.bottoni94@gmail.com
 */

public class Tombola extends JFrame {

    private Container container;
    private BorderLayout layout;

    private JMenuBar bar;
    private JMenu fileMenu, colorMenu, optionMenu, infoMenu, textColorMenu, buttonColorMenu, imageColorMenu;
    private JMenuItem reset, exit, restore, info;

    private ButtonGroup buttonRadioImageColorGroup, buttonRadioButtonColorGroup, buttonRadioTextColorGroup;
    private JRadioButtonMenuItem redImage, redwImage, orangeImage, greenwImage, bluewImage, blackImage;
    private JRadioButtonMenuItem redButton, orangeButton, greenwButton, bluewButton, blackButton;
    private JRadioButtonMenuItem whiteText, blackText;

    private JPanel titlePanel, inTitlePanel, billboardPanel;
    private JPanel box1Panel, box2Panel, box3Panel, box4Panel, box5Panel, box6Panel;
    private JPanel eastPanel;
    private JPanel imagePanel, extractPanel;

    private JLabel titleLabel, logoLabel, imageLabel, imageSubLabel1, imageSubLabel2;

    private JButton button[][];
    private JButton extractButton;

    private String val1, val2;

    private java.util.List<String>  numberList, randomList, colorList;

    private SaveToFile saveToFile;

    //Default Color
    private String colorPath = "red";
    private Color buttonColor = new Color(255, 0, 0);
    private Color textColor = Color.white;

    private int thickness = 4;

    private ButtonHandler handler;
    private ColorHandler colorHandler;


    public Tombola() {

        //Handler
        handler = new ButtonHandler();
        colorHandler = new ColorHandler();

        //SaveToFile
        saveToFile = new SaveToFile();
        saveToFile.createFile();

        //PreSet
        colorList = saveToFile.readColor();
        if(colorList.isEmpty() || colorList.size() < 3) {
            colorList.add("red");
            colorList.add("red");
            colorList.add("white");
            saveToFile.writeColorList(colorList);
        }
        switch (colorList.get(0)) {
            case "red":
            case "redw":
            case "orange":
            case "bluew":
            case "greenw":
            case "black":
                colorPath = colorList.get(0);
                break;
            default:
                colorPath = "red";
                break;
        }
        switch (colorList.get(1)) {
            case "red":
                buttonColor = new Color(255, 0, 0);
                break;
            case "orange":
                buttonColor = new Color(255, 164, 60);
                break;
            case "blue":
                buttonColor = new Color(0, 0, 255);
                break;
            case "green":
                buttonColor = new Color(13, 224, 69);
                break;
            case "black":
                buttonColor = new Color(00, 00, 00);
                break;
            default:
                buttonColor = new Color(255, 0, 0);
                break;
        }
        switch (colorList.get(2)) {
            case "black":
                textColor = Color.black;
                break;
            default:
                textColor = Color.white;
                break;
        }

        //Liste
        numberList = new ArrayList<>();
        randomList = new ArrayList<>();

        //Container
        container = getContentPane();
        layout = new BorderLayout();
        container.setLayout(layout);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(colorPath + "/T.png")));

        //MenùBar
        bar = new JMenuBar();
        setJMenuBar(bar);

        fileMenu = new JMenu("File");
        fileMenu.setMnemonic('F');
        bar.add(fileMenu);

        colorMenu = new JMenu("Colori");
        colorMenu.setMnemonic('C');
        bar.add(colorMenu);

        //Colori Immagini
        buttonRadioImageColorGroup = new ButtonGroup();
        redImage = new JRadioButtonMenuItem("Rosso");
        if(colorList.get(0).equals("red")) {
            redImage.setSelected(true);
        }
        redImage.addActionListener(colorHandler);
        buttonRadioImageColorGroup.add(redImage);
        redwImage = new JRadioButtonMenuItem("Rosso - Bianco");
        if(colorList.get(0).equals("redw")) {
            redwImage.setSelected(true);
        }
        redwImage.addActionListener(colorHandler);
        buttonRadioImageColorGroup.add(redwImage);
        orangeImage = new JRadioButtonMenuItem("Arancione");
        if(colorList.get(0).equals("orange")) {
            orangeImage.setSelected(true);
        }
        orangeImage.addActionListener(colorHandler);
        buttonRadioImageColorGroup.add(orangeImage);
        greenwImage = new JRadioButtonMenuItem("Verde");
        if(colorList.get(0).equals("greenw")) {
            greenwImage.setSelected(true);
        }
        greenwImage.addActionListener(colorHandler);
        buttonRadioImageColorGroup.add(greenwImage);
        bluewImage = new JRadioButtonMenuItem("Blu");
        if(colorList.get(0).equals("bluew")) {
            bluewImage.setSelected(true);
        }
        bluewImage.addActionListener(colorHandler);
        buttonRadioImageColorGroup.add(bluewImage);
        blackImage = new JRadioButtonMenuItem("Nero");
        if(colorList.get(0).equals("black")) {
            blackImage.setSelected(true);
        }
        blackImage.addActionListener(colorHandler);
        buttonRadioImageColorGroup.add(blackImage);

        //Colori Tasti
        buttonRadioButtonColorGroup = new ButtonGroup();
        redButton = new JRadioButtonMenuItem("Rosso");
        if(colorList.get(1).equals("red")) {
            redButton.setSelected(true);
        }
        buttonRadioButtonColorGroup.add(redButton);
        redButton.addActionListener(colorHandler);
        orangeButton = new JRadioButtonMenuItem("Arancione");
        if(colorList.get(1).equals("orange")) {
            orangeButton.setSelected(true);
        }
        orangeButton.addActionListener(colorHandler);
        buttonRadioButtonColorGroup.add(orangeButton);
        greenwButton = new JRadioButtonMenuItem("Verde");
        if(colorList.get(1).equals("green")) {
            greenwButton.setSelected(true);
        }
        greenwButton.addActionListener(colorHandler);
        buttonRadioButtonColorGroup.add(greenwButton);
        bluewButton = new JRadioButtonMenuItem("Blu");
        if(colorList.get(1).equals("blue")) {
            bluewButton.setSelected(true);
        }
        bluewButton.addActionListener(colorHandler);
        buttonRadioButtonColorGroup.add(bluewButton);
        blackButton = new JRadioButtonMenuItem("Nero");
        if(colorList.get(1).equals("black")) {
            blackButton.setSelected(true);
        }
        blackButton.addActionListener(colorHandler);
        buttonRadioButtonColorGroup.add(blackButton);

        //Colore Testo
        buttonRadioTextColorGroup = new ButtonGroup();
        whiteText = new JRadioButtonMenuItem("Bianco");
        if(colorList.get(2).equals("white")) {
            whiteText.setSelected(true);
        }
        whiteText.addActionListener(colorHandler);
        buttonRadioTextColorGroup.add(whiteText);
        blackText = new JRadioButtonMenuItem("Nero");
        if(colorList.get(2).equals("black")) {
            blackText.setSelected(true);
        }
        buttonRadioTextColorGroup.add(blackText);
        blackText.addActionListener(colorHandler);


        imageColorMenu = new JMenu("Numeri");
        colorMenu.add(imageColorMenu);
        imageColorMenu.add(redImage);
        imageColorMenu.add(redwImage);
        imageColorMenu.add(orangeImage);
        imageColorMenu.add(bluewImage);
        imageColorMenu.add(greenwImage);
        imageColorMenu.add(blackImage);

        buttonColorMenu = new JMenu("Sfondo Tasti");
        colorMenu.add(buttonColorMenu);
        buttonColorMenu.add(redButton);
        buttonColorMenu.add(orangeButton);
        buttonColorMenu.add(bluewButton);
        buttonColorMenu.add(greenwButton);
        buttonColorMenu.add(blackButton);

        textColorMenu = new JMenu("Numeri Tasti");
        colorMenu.add(textColorMenu);
        textColorMenu.add(whiteText);
        textColorMenu.add(blackText);

        optionMenu = new JMenu("Opzioni");
        optionMenu.setMnemonic('O');
        bar.add(optionMenu);

        infoMenu = new JMenu("Info");
        infoMenu.setMnemonic('I');
        bar.add(infoMenu);

        reset = new JMenuItem("Reset");
        optionMenu.add(reset);
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
                setImage("");
            }
        });

        restore = new JMenuItem("Ripristina");
        optionMenu.add(restore);
        restore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               numberList = saveToFile.readList();
               setRandomList();
            }
        });

        exit = new JMenuItem("Esci");
        fileMenu.add(exit);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        info = new JMenuItem("Info");
        infoMenu.add(info);
        ImageIcon infoicon = new ImageIcon(getClass().getResource(colorPath + "/T.png"));
        info.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Realizzato da: Bottoni Simone\n" +
                        "Contatti: simone.bottoni94@gmail.com", "Informazioni", JOptionPane.INFORMATION_MESSAGE, infoicon);
            }
        });


        //Panel
        titlePanel = new JPanel();
        titlePanel.setLayout(new BorderLayout());
        container.add(titlePanel, BorderLayout.NORTH);

        inTitlePanel = new JPanel();
        inTitlePanel.setLayout(new BorderLayout());
        titlePanel.add(inTitlePanel, BorderLayout.CENTER);

        billboardPanel = new JPanel();
        billboardPanel.setLayout(new GridLayout(3, 2, 25,25));
        container.add(billboardPanel, BorderLayout.CENTER);

        eastPanel = new JPanel();
        eastPanel.setLayout(new BorderLayout(20,20));
        container.add(eastPanel, BorderLayout.EAST);
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        eastPanel.setPreferredSize(new Dimension((275), dimension.height));

        box1Panel = new JPanel();
        box1Panel.setLayout(new GridLayout(3, 5, 10, 10));
        billboardPanel.add(box1Panel);

        box2Panel = new JPanel();
        box2Panel.setLayout(new GridLayout(3, 5, 10, 10));
        billboardPanel.add(box2Panel);

        box3Panel = new JPanel();
        box3Panel.setLayout(new GridLayout(3, 5, 10, 10));
        billboardPanel.add(box3Panel);

        box4Panel = new JPanel();
        box4Panel.setLayout(new GridLayout(3, 5, 10, 10));
        billboardPanel.add(box4Panel);

        box5Panel = new JPanel();
        box5Panel.setLayout(new GridLayout(3, 5, 10, 10));
        billboardPanel.add(box5Panel);

        box6Panel = new JPanel();
        box6Panel.setLayout(new GridLayout(3, 5, 10, 10));
        billboardPanel.add(box6Panel);

        imagePanel = new JPanel();
        imagePanel.setLayout(new BorderLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Numero Estratto: ");
        titledBorder.setTitlePosition(TitledBorder.ABOVE_TOP);
        imagePanel.setBorder(titledBorder);
        eastPanel.add(imagePanel, BorderLayout.CENTER);

        extractPanel = new JPanel();
        extractPanel.setLayout(new BorderLayout());
        eastPanel.add(extractPanel, BorderLayout.SOUTH);


        //Bordi Panel
        eastPanel.setBorder(new EmptyBorder(0, 0, 20, 20));
        box1Panel.setBorder(new CompoundBorder(new EmptyBorder(20, 20, 0, 0), BorderFactory.createLineBorder(buttonColor, thickness)));
        box2Panel.setBorder(new CompoundBorder(new EmptyBorder(20, 0, 0, 20), BorderFactory.createLineBorder(buttonColor, thickness)));
        box3Panel.setBorder(new CompoundBorder(new EmptyBorder(10, 20, 10, 0), BorderFactory.createLineBorder(buttonColor, thickness)));
        box4Panel.setBorder(new CompoundBorder(new EmptyBorder(10, 0, 10, 20), BorderFactory.createLineBorder(buttonColor, thickness)));
        box5Panel.setBorder(new CompoundBorder(new EmptyBorder(0, 20, 20, 0), BorderFactory.createLineBorder(buttonColor, thickness)));
        box6Panel.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 20, 20), BorderFactory.createLineBorder(buttonColor, thickness)));

        inTitlePanel.setBorder(new EmptyBorder(0, ((dimension.width/7)), 0, 0));


        //Label
        ImageIcon imageIcon = new ImageIcon(getClass().getResource(colorPath + "/centro.png"));
        titleLabel = new JLabel(imageIcon);
        titlePanel.add(titleLabel, BorderLayout.WEST);

        ImageIcon imageLogo = new ImageIcon(getClass().getResource(colorPath + "/tombola.png"));
        logoLabel = new JLabel(imageLogo);
        inTitlePanel.add(logoLabel, BorderLayout.WEST);

        imageLabel = new JLabel();
        imageLabel.setLayout(new BoxLayout(imageLabel, BoxLayout.X_AXIS));
        imagePanel.add(imageLabel, BorderLayout.CENTER);
        imageSubLabel1 = new JLabel();
        imageSubLabel2 = new JLabel();
        imageLabel.add(imageSubLabel1);
        imageLabel.add(imageSubLabel2);


        //Button
        button = new JButton[9][10];
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 5; j++) {
                if(i == 0) {
                    button[i][j] = new JButton( "" + (j+1));
                    box1Panel.add(button[i][j]);
                }
                else {
                    button[i][j] = new JButton("" + i + "" + (j+1));
                    box1Panel.add(button[i][j]);
                }
            }
        }
        for(int i = 0; i < 3; i++) {
            for(int j = 5; j < 10; j++) {
                if(i == 0) {
                    button[i][j] = new JButton( "" + (j+1));
                    box2Panel.add(button[i][j]);
                }
                else {
                    if(j == 9) {
                        button[i][j] = new JButton("" + (i+1) + "" + 0);
                        box2Panel.add(button[i][j]);
                    }
                    else {
                        button[i][j] = new JButton("" + i + "" + (j+1));
                        box2Panel.add(button[i][j]);
                    }
                }

            }
        }
        for(int i = 3; i < 6; i++) {
            for(int j = 0; j < 5; j++) {
                button[i][j] = new JButton( "" + i + "" + (j+1));
                box3Panel.add(button[i][j]);

            }
        }
        for(int i = 3; i < 6; i++) {
            for(int j = 5; j < 10; j++) {
                if(j == 9) {
                    button[i][j] = new JButton("" + (i+1) + "" + 0);
                    box4Panel.add(button[i][j]);
                }
                else {
                    button[i][j] = new JButton("" + i + "" + (j+1));
                    box4Panel.add(button[i][j]);
                }

            }
        }
        for(int i = 6; i < 9; i++) {
            for(int j = 0; j < 5; j++) {
                button[i][j] = new JButton( "" + i + "" + (j+1));
                box5Panel.add(button[i][j]);

            }
        }
        for(int i = 6; i < 9; i++) {
            for(int j = 5; j < 10; j++) {
                if(j == 9) {
                    button[i][j] = new JButton("" + (i+1) + "" + 0);
                    box6Panel.add(button[i][j]);
                }
                else {
                    button[i][j] = new JButton("" + i + "" + (j+1));
                    box6Panel.add(button[i][j]);
                }

            }
        }

        for(int i = 0; i < 9; i++) {
            for(int j = 0; j < 10; j++) {
                randomList.add(button[i][j].getText());
                button[i][j].setBackground(Color.WHITE);
                button[i][j].addActionListener(handler);
                button[i][j].setFont(new Font("Arial", Font.PLAIN, 40));
            }
        }

        extractButton = new JButton("Estrai");
        extractPanel.add(extractButton);
        extractButton.addActionListener(handler);

        dimension = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(dimension.width, dimension.height);
        setExtendedState(JFrame.MAXIMIZED_BOTH); //FullScreen
        setVisible(true);
        setTitle("Tombola");

    }

    public static void main(String[] args) {
        Tombola tombola = new Tombola();
        tombola.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private class ButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            if(event.getSource() == extractButton) {
                if(randomList.size() != 0) {
                    Random random = new Random();
                    int extractNumber = random.nextInt(randomList.size());
                    String numberExtrR = randomList.get(extractNumber);
                    randomList.remove(extractNumber);
                    numberList.add(numberExtrR);
                    saveToFile.writeList(numberList);
                    for (int i = 0; i < 9; i++) {
                        for (int j = 0; j < 10; j++) {
                            if (numberExtrR.equals(button[i][j].getText())) {
                                button[i][j].setForeground(textColor);
                                button[i][j].setBackground(buttonColor);
                                setImage(numberExtrR);
                            }
                        }
                    }
                }
            }
            else {
                for (int i = 0; i < 9; i++) {
                    for (int j = 0; j < 10; j++) {
                        if (event.getSource() == button[i][j]) {
                            if (numberList.contains(button[i][j].getText())) {
                                button[i][j].setBackground(Color.WHITE);
                                button[i][j].setForeground(Color.black);
                                numberList.remove(button[i][j].getText());
                                randomList.add(button[i][j].getText());
                                if((numberList.size()-1) == -1) {
                                    setImage("");
                                }
                                else {
                                    setImage(numberList.get(numberList.size()-1));
                                }
                            } else {
                                button[i][j].setBackground(buttonColor);
                                button[i][j].setForeground(textColor);
                                numberList.add(button[i][j].getText());
                                randomList.remove(button[i][j].getText());
                                saveToFile.writeList(numberList);
                                setImage(button[i][j].getText());
                            }
                        }
                    }
                }
            }

        }
    }

    private class ColorHandler implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            if(event.getSource() == redImage) {
                redImage.setSelected(true);
                colorPath = "red";
                colorList.remove(0);
                colorList.add(0, colorPath);
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == redwImage) {
                redwImage.setSelected(true);
                colorPath = "redw";
                colorList.remove(0);
                colorList.add(0, colorPath);
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == orangeImage) {
                orangeImage.setSelected(true);
                colorPath = "orange";
                colorList.remove(0);
                colorList.add(0, colorPath);
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == bluewImage) {
                bluewImage.setSelected(true);
                colorPath = "bluew";
                colorList.remove(0);
                colorList.add(0, colorPath);
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == greenwImage) {
                greenwImage.setSelected(true);
                colorPath = "greenw";
                colorList.remove(0);
                colorList.add(0, colorPath);
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == blackImage) {
                blackImage.setSelected(true);
                colorPath = "black";
                colorList.remove(0);
                colorList.add(0, colorPath);
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }

            if(event.getSource() == redButton) {
                redButton.setSelected(true);
                buttonColor = new Color(255, 0, 0);
                colorList.remove(1);
                colorList.add(1, "red");
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == orangeButton) {
                orangeButton.setSelected(true);
                buttonColor = new Color(255, 164, 60);
                colorList.remove(1);
                colorList.add(1, "orange");
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == bluewButton) {
                bluewButton.setSelected(true);
                buttonColor = new Color(0, 0, 255);
                colorList.remove(1);
                colorList.add(1, "blue");
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == greenwButton) {
                greenwButton.setSelected(true);
                buttonColor = new Color(13, 224, 69);
                colorList.remove(1);
                colorList.add(1, "green");
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == blackButton) {
                blackButton.setSelected(true);
                buttonColor = new Color(00, 00, 00);
                colorList.remove(1);
                colorList.add(1, "black");
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }

            if(event.getSource() == whiteText) {
                whiteText.setSelected(true);
                textColor = Color.white;
                colorList.remove(2);
                colorList.add(2, "white");
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }
            if(event.getSource() == blackText) {
                blackText.setSelected(true);
                textColor = Color.black;
                colorList.remove(2);
                colorList.add(2, "black");
                saveToFile.writeColorList(colorList);
                changeColorAll();
            }

        }
    }

    private void changeColorAll() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 10; j++) {
                if (numberList.contains(button[i][j].getText())) {
                    button[i][j].setForeground(textColor);
                    button[i][j].setBackground(buttonColor);
                    setImage("" + val1 + "" + val2 + "");
                }
            }
        }
        ImageIcon imageIcon = new ImageIcon(getClass().getResource(colorPath + "/centro.png"));
        titleLabel.setIcon(imageIcon);
        ImageIcon imageLogo = new ImageIcon(getClass().getResource(colorPath + "/tombola.png"));
        logoLabel.setIcon(imageLogo);
        box1Panel.setBorder(new CompoundBorder(new EmptyBorder(20, 20, 0, 0), BorderFactory.createLineBorder(buttonColor, thickness)));
        box2Panel.setBorder(new CompoundBorder(new EmptyBorder(20, 0, 0, 20), BorderFactory.createLineBorder(buttonColor, thickness)));
        box3Panel.setBorder(new CompoundBorder(new EmptyBorder(10, 20, 10, 0), BorderFactory.createLineBorder(buttonColor, thickness)));
        box4Panel.setBorder(new CompoundBorder(new EmptyBorder(10, 0, 10, 20), BorderFactory.createLineBorder(buttonColor, thickness)));
        box5Panel.setBorder(new CompoundBorder(new EmptyBorder(0, 20, 20, 0), BorderFactory.createLineBorder(buttonColor, thickness)));
        box6Panel.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 20, 20), BorderFactory.createLineBorder(buttonColor, thickness)));
    }

    private void setImage(String image) {
        ImageIcon imageIcon1, imageIcon2;
        if(image.equals("")) {
           imageIcon1  = new ImageIcon();
           imageIcon2  = new ImageIcon();
        }
        else {
            if(image.length() == 1) {
                val1 = Character.toString('0');
                val2 = Character.toString(image.charAt(0));
            }
            else {
                val1 = Character.toString(image.charAt(0));
                val2 = Character.toString(image.charAt(1));
            }
            imageIcon1 = new ImageIcon(getClass().getResource(colorPath + "/" + val1 + ".png"));
            imageIcon2 = new ImageIcon(getClass().getResource(colorPath + "/" + val2 + ".png"));
        }
        imageSubLabel1.setIcon(imageIcon1);
        imageSubLabel2.setIcon(imageIcon2);
    }

    //Riempe la lista contraria dove vado a togliere i numeri una volta estratti
    private void setRandomList() {
        if(!numberList.isEmpty()) {
            for(int k = 1; k < 91; k++) {
                if(numberList.contains("" + k + "")) {
                    randomList.remove("" + k + "");
                    for (int i = 0; i < 9; i++) {
                        for (int j = 0; j < 10; j++) {
                            if (("" + k + "").equals(button[i][j].getText())) {
                                button[i][j].setBackground(buttonColor);
                                button[i][j].setForeground(textColor);
                            }
                        }
                    }
                }
            }
            setImage(numberList.get(numberList.size()-1));
        }
    }

    private void reset() {
        saveToFile.writeEmpty();
        randomList.clear();
        numberList.clear();
        for(int i = 0; i < 9; i++) {
            for(int j = 0; j < 10; j++) {
                randomList.add(button[i][j].getText());
                button[i][j].setBackground(Color.WHITE);
                button[i][j].setForeground(Color.black);
            }
        }
    }

}