import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright (C) 2017-2018  Bottoni Simone - simone.bottoni94@gmail.com
 */

public class SaveToFile {

    private String path = System.getProperty("user.home") + File.separator + "Tombola";
    private final File fileDir = new File(path);
    private final File file = new File(path + File.separator +"Tombola"); //Numeri
    private final File color = new File(path + File.separator +"Color"); //Colori

    public SaveToFile() {
    }

    public void createFile(){
        if(!fileDir.exists()){
            fileDir.mkdirs();
        }
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(!color.exists()){
            try {
                color.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeEmpty(){
        createFile();
        try{
            PrintWriter printWriter = new PrintWriter(new FileWriter(file));
            printWriter.write("");
            printWriter.flush();
            printWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //Legge i numeri
    public List<String> readList() {
        createFile();
        try {
            List<String> list = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String read = "";
            while(true) {
                read = bufferedReader.readLine();
                if(read == null) {
                    break;
                }
                else {
                    if(!list.contains(read)) {
                        list.add(read);
                    }
                }
            }
            return list;
        } catch (FileNotFoundException e) {
            return new ArrayList<>();
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    //Scrive i numeri
    public void writeList(List<String> list){
        createFile();
        try{
            PrintWriter printWriter = new PrintWriter(new FileWriter(file));
            Iterator<String> it = list.iterator();
            while(it.hasNext()) {
                printWriter.write(it.next() + "\n");
            }
            printWriter.flush();
            printWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //Scrive i 3 colori
    public void writeColorList(List<String> list){
        createFile();
        try{
            PrintWriter printWriter = new PrintWriter(new FileWriter(color));
            Iterator<String> it = list.iterator();
            while(it.hasNext()) {
                printWriter.write(it.next() + "\n");
            }
            printWriter.flush();
            printWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //Legge i 3 colori
    public List<String> readColor() {
        createFile();
        List<String> list = new ArrayList<>();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(color));
            String read = "";
            while(true) {
                read = bufferedReader.readLine();
                if(read == null) {
                    break;
                }
                else {
                    list.add(read);
                }
            }
            return list;
        } catch (FileNotFoundException e) {
            return new ArrayList<>();
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }
}